#!/bin/bash
set -eEo pipefail  # fail if anything returns badly

# Usage: initial -h

show_help() {
    echo 'Usage: initial [-n <fqdn>] [-u] [-d] [-o "<ip1:host1+host11,ip2:host2,...>"] [-h] [-p] [--ntp_pool <ntp_pool>]'
    echo '    -h|--help                     will print this help'
    echo '    -d|--debug                    run this script in debug mode'
    echo '    --disable_thp                 disable transparent huge pages'
    echo '    -n|--hostname <fqdn>          to set fully qualified hostname'
    echo '    --ntp_pool <ntp_pool>         NTP server to use'
    echo '    -o|--hosts "<ip1:host1+host11,ip2:host2,(...)>"   List of IPs and names to append to /etc/hosts'
    echo '    -p|--allow_ssh_root_password  allow user root SSH with password'
    echo '    --preserve_hostname           preserve hostname if cloud instance'
    echo '    -u|--update                   will update system, kernel, and reboot'
}

die() {
    printf '%s\n' "$1" >&2
    exit 1
}


# http://mywiki.wooledge.org/BashFAQ/035
do_update_restart=
set_hostname=`hostname -f`
is_debug=
etc_hosts=
allow_ssh_root_password=
ntp_pool="0.fr.pool.ntp.org"
preserve_hostname='false'
disable_thp='false'
while :; do
    case $1 in
        -n|--hostname)
            if [ "$2" ]; then
                set_hostname=$2
                shift
            else
                show_help
                die 'ERROR: "-n|--hostname" requires a non-empty option argument.'
            fi
            ;;
        --hostname=?*)
            set_hostname=${1#*=}  # Delete everything up to "=" and assign the remainder.
            ;;
        --hostname=)  # Handle the case of an empty --hostname=
            show_help
            die 'ERROR: "--hostname" requires a non-empty option argument.'
            ;;
        -h|-\?|--help)
            show_help
            exit 0
            ;;
        -u|--update)
            do_update_restart=yes
            ;;
        -d|--debug)
            # is_debug=$((is_debug + 1))  # Each -v adds 1 to verbosity.
            is_debug=yes
            ;;
        -o|--hosts)
            if [ "$2" ]; then
                etc_hosts=$2
                shift
            else
                show_help
                die 'ERROR: "-n|--etc_hosts" requires a non-empty option argument.'
            fi
            ;;
        --hosts=?*)
            etc_hosts=${1#*=}  # Delete everything up to "=" and assign the remainder.
            ;;
        --hosts=)  # Handle the case of an empty --hostname=
            show_help
            die 'ERROR: "--etc_hosts" requires a non-empty option argument.'
            ;;
        -p|--allow_ssh_root_password)
            allow_ssh_root_password=yes
            ;;
        --ntp_pool)
            if [ "$2" ]; then
                ntp_pool=$2
                shift
            else
                show_help
                die 'ERROR: "--ntp" requires a non-empty option argument.'
            fi
            ;;
        --ntp_pool=?*)
            ntp_pool=${1#*=}  # Delete everything up to "=" and assign the remainder.
            ;;
        --ntp_pool=)  # Handle the case of an empty --ntp=
            show_help
            die 'ERROR: "---ntp" requires a non-empty option argument.'
            ;;
        --preserve_hostname)
            preserve_hostname='true'
            ;;
        --disable_thp)
            disable_thp='true'
            ;;
        --)  # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)  # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done


# A few sanity checks
rval=0
test -z "$set_hostname" && rval=2
test -z "$ntp_pool" && rval=3

test $rval -gt 0 && show_help && exit $rval

test "$is_debug" = "yes" && set -x


architecture=`uname -i`
machine_arch=`uname -m`

# Debian specifics:
#     localectl list-locales
#     dpkg-reconfigure locales
# or  apt install locales-all
cat /etc/default/locale
cat >/etc/default/locale <<EOF
LANG="en_US.UTF-8"
EOF
export LANG=en_US.UTF-8
# cat >/etc/default/locale <<EOF
# LANG="C.UTF-8"
# EOF
# export LANG=C.UTF-8


# Prevent automatic start of services at installation
# howto: https://major.io/2014/06/26/install-debian-packages-without-starting-daemons/
# discussion: https://news.ycombinator.com/item?id=7952838
# documentation: https://people.debian.org/~hmh/invokerc.d-policyrc.d-specification.txt
cat >/usr/sbin/policy-rc.d << EOF
#!/bin/sh
echo "All runlevel operations denied by policy" >&2
exit 101
EOF


apt update

# apt remove -y sendmail
apt install -y curl wget rsync sudo xfsprogs patch man-db lsof telnet ruby mosh dnsutils binutils net-tools gnupg2 debconf-utils software-properties-common tuned
apt install -y gdisk || true

pushd /tmp
echo "deb https://mistertea.github.io/debian-et/debian-source/ bookworm main" | tee /etc/apt/sources.list.d/et.list
curl -sS https://mistertea.github.io/debian-et/et.gpg | apt-key add -
wget https://apt.puppet.com/puppet7-release-bookworm.deb
dpkg -i puppet7-release-bookworm.deb
rm -f puppet7-release-bookworm.deb
apt update
popd

# debconf-show postfix
# debconf-get-selections | grep postfix
echo "postfix postfix/mailname string $set_hostname" | debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
apt install -y postfix


# Network time
# Necessary for some Online servers. Bad time prevents DNSSEC and all DNS resolutions to success
dpkg-query -W ntpdate && systemctl disable --now ntpdate || true
dpkg-query -W ntp && systemctl disable --now ntp || true
dpkg-query -W ntp && apt purge ntp -y || true
timedatectl set-timezone Etc/UTC
timedatectl set-ntp true || true
apt install -y chrony
sed -i --follow-symlinks 's/^server .*//' /etc/chrony/chrony.conf
sed -i --follow-symlinks "s/^pool .*/pool ${ntp_pool} iburst/" /etc/chrony/chrony.conf
grep -q '^pool ' /etc/chrony/chrony.conf || echo "pool ${ntp_pool} iburst" >>/etc/chrony/chrony.conf
systemctl enable chrony
systemctl restart chrony


test "$disable_thp" = 'false' && thp_setting='always' || thp_setting='madvise'

# grub_cmdline="`grep 'GRUB_CMDLINE_LINUX=' /etc/sysconfig/grub`" || true
# if test -n "`echo $grub_cmdline |grep transparent_hugepage`" ; then
#     grub_cmdline="`echo $grub_cmdline |sed \"s,transparent_hugepage=\w*,transparent_hugepage=${thp_setting},\"`"
# else
#     grub_cmdline="`echo $grub_cmdline |sed \"s,=\\",=\\"transparent_hugepage=${thp_setting} ,\"`"
# fi
# sed -i.orig --follow-symlinks "s#GRUB_CMDLINE_LINUX=.*#${grub_cmdline}#" /etc/sysconfig/grub
#
# test -e /boot/efi/EFI/redhat/grub.cfg && grub_file='/boot/efi/EFI/redhat/grub.cfg' || grub_file='/boot/grub2/grub.cfg'
# grub2-mkconfig -o $grub_file

echo $thp_setting >/sys/kernel/mm/transparent_hugepage/enabled

if test -e '/etc/tuned' ; then
    mkdir -p /etc/tuned/balanced-no_thp
    cat >/etc/tuned/balanced-no_thp/tuned.conf <<EOF
[main]
include=balanced

[vm]
transparent_hugepages=${thp_setting}
EOF

    tuned-adm profile balanced-no_thp
fi

cat /sys/kernel/mm/transparent_hugepage/enabled


# Remove Firewalld and use traditionnal Iptables. Mostly because of Docker and Podman
systemctl disable --now firewalld || true
echo "iptables-persistent iptables-persistent/autosave_v4 boolean false" | debconf-set-selections
echo "iptables-persistent iptables-persistent/autosave_v6 boolean false" | debconf-set-selections
apt install -y iptables iptables-persistent
systemctl enable netfilter-persistent.service
systemctl restart netfilter-persistent.service || true


apt install et -y
systemctl enable --now et
iptables -I INPUT 1 -p tcp -m conntrack --ctstate NEW -m tcp --dport 2022 -j ACCEPT


# How to revert to traditionnal interface names
# https://wiki.archlinux.org/index.php/Network_configuration#Revert_to_traditional_interface_names

test -e /sys/class/net/eth0 && intf_pub=eth0
test -e /sys/class/net/eno1 && intf_pub=eno1
test -e /sys/class/net/ens2 && intf_pub=ens2
test -e /sys/class/net/ens1 && intf_pub=ens1
test -e /sys/class/net/em1 && intf_pub=em1
test -e /sys/class/net/ens160 && intf_pub=ens160
test -e /sys/class/net/ens3 && intf_pub=ens3
test -e /sys/class/net/enp3s0f0 && intf_pub=enp3s0f0
intf_priv=`ip addr |grep -P '^\d' |grep -vP "^\d+: *${intf_pub}" |grep -vP "^\d+: *lo" |grep -vP "^\d+: *veth" |grep -vP "^\d+: *kube-" |grep -vP "^\d+: *kilo\d" |grep -P " +state +UP" |ruby -ne 'puts /^\d+: *(.+?):/.match($_)[1]' || true`

test -n "$intf_pub" && ip_pub=`ip addr show dev $intf_pub |grep  -P "inet \d" |ruby -ne 'puts $_.match(/inet +(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(\/\d{1,2})? /)[1]' |head -1`
test -n "$intf_priv" && ip_priv=`ip addr show dev $intf_priv |grep  -P "inet \d" |ruby -ne 'puts $_.match(/inet +(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(\/\d{1,2})? /)[1]' |head -1`
echo $intf_pub $ip_pub
echo $intf_priv $ip_priv

if test -n "$ip_priv"; then
    echo "$ip_priv $set_hostname" >>/etc/hosts
else
    echo "$ip_pub $set_hostname" >>/etc/hosts
fi
if test -n "$etc_hosts"; then
    echo "$etc_hosts" |tr ':' ' ' |tr '+' ' ' |tr ',' "\n" >>/etc/hosts
fi
cat /etc/hosts


# Change hostname
echo $set_hostname >/etc/hostname
hostname `head -1 /etc/hostname`
# https://aws.amazon.com/premiumsupport/knowledge-center/linux-static-hostname-rhel7-centos7/
if test -f /etc/cloud/cloud.cfg; then
    set +e
    grep -q '^preserve_hostname:' /etc/cloud/cloud.cfg
    if test $? -ne 0; then
        echo -e "\npreserve_hostname: ${preserve_hostname}\n" >>/etc/cloud/cloud.cfg
    else
        sed -i --follow-symlinks "s/^preserve_hostname:.*/preserve_hostname: ${preserve_hostname}/g" /etc/cloud/cloud.cfg
    fi
    grep -q '^disable_root:' /etc/cloud/cloud.cfg
    if test $? -ne 0; then
        echo -e "\ndisable_root: 0\n" >>/etc/cloud/cloud.cfg
    else
        sed -i --follow-symlinks 's/^disable_root:.*/disable_root: 0/g' /etc/cloud/cloud.cfg
    fi
    grep -q '^ssh_deletekeys:' /etc/cloud/cloud.cfg
    if test $? -ne 0; then
        echo -e "\nssh_deletekeys: 1\n" >>/etc/cloud/cloud.cfg
    else
        sed -i --follow-symlinks 's/^ssh_deletekeys:.*/ssh_deletekeys: 1/g' /etc/cloud/cloud.cfg
    fi
    set -eEo pipefail
fi


mkdir -p /root/.ssh
chmod 700 /root/.ssh
pushd /root/.ssh
touch /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
sed -i --follow-symlinks 's/^.*command.*Please login as the user .*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys
sed -i --follow-symlinks 's/^.*command.*Please login as the user .*ssh-ed25519/ssh-ed25519/' /root/.ssh/authorized_keys
grep -q '^ssh-ed25519 .* alex@aair' /root/.ssh/authorized_keys || echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA7SFdLegC94rBKtcCEE4QCrKSpk19Xpq+6GY/s8kvyl alex@aair' >> /root/.ssh/authorized_keys
grep -q '^ssh-ed25519 .* alex@a13m' /root/.ssh/authorized_keys || echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILh0RUL3Os6zrTzIR5mJcaHp0Dz9gaDAqSL6xxlsJDQH alex@a13m' >> /root/.ssh/authorized_keys
popd

sed -i.orig --follow-symlinks 's/^.*PermitRootLogin.*$//g' /etc/ssh/sshd_config
if [ "$allow_ssh_root_password" = "yes" ]; then
    echo 'PermitRootLogin yes' >>/etc/ssh/sshd_config
else
    echo 'PermitRootLogin without-password' >>/etc/ssh/sshd_config
fi
systemctl reload ssh || true
systemctl enable --now ssh


# Install Puppet 7 agent
puppet_version='7.33.0-1bookworm'; apt install -y puppet-agent=${puppet_version}
apt-mark hold puppet
systemctl disable --now puppet pxp-agent


netfilter-persistent save


if [ "$do_update_restart" = "yes" ]; then
    # Update because of Heatbleed bug
    apt upgrade -y  # Don't call this after the first puppetize !!! It updates all packages even if we wanted to install an old version !! Perhaps we should use "dnf versionlock" http://serverfault.com/a/546101 ?

    # is_ok=wait
    # while test "$is_ok" != 'ok'; do
    # echo "FINISH. READY FOR REBOOT ? TYPE 'ok' or ctrl+c to cancel"
    # read is_ok
    # done

    # get a chance to exit with code 0 for Rundeck
    #shutdown -r --no-wall now
    nohup bash -c "sleep 5; shutdown -r --no-wall now" &
fi

exit $rval
