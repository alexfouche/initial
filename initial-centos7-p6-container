#!/bin/bash
set -eEo pipefail  # fail if anything returns badly

# Usage: initial -h

show_help() {
    echo 'Usage: initial [-d] [-o "<ip1:host1+host11,ip2:host2,...>"] [-h]'
    echo '    -h|--help                     will print this help'
    echo '    -d|--debug                    run this script in debug mode'
    echo '    -o|--hosts "<ip1:host1+host11,ip2:host2,(...)>"   List of IPs and names to append to /etc/hosts'
}

die() {
    printf '%s\n' "$1" >&2
    exit 1
}


# http://mywiki.wooledge.org/BashFAQ/035
is_debug=
etc_hosts=
while :; do
    case $1 in
        -h|-\?|--help)
            show_help
            exit 0
            ;;
        -u|--update)
            do_update_restart=yes
            ;;
        -d|--debug)
            # is_debug=$((is_debug + 1))  # Each -v adds 1 to verbosity.
            is_debug=yes
            ;;
        -o|--hosts)
            if [ "$2" ]; then
                etc_hosts=$2
                shift
            else
                show_help
                die 'ERROR: "-n|--etc_hosts" requires a non-empty option argument.'
            fi
            ;;
        --hosts=?*)
            etc_hosts=${1#*=}  # Delete everything up to "=" and assign the remainder.
            ;;
        --hosts=)  # Handle the case of an empty --hostname=
            show_help
            die 'ERROR: "--etc_hosts" requires a non-empty option argument.'
            ;;
        --)  # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)  # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done


# A few sanity checks
rval=0
test $rval -gt 0 && show_help && exit $rval

test "$is_debug" = "yes" && set -x


architecture=`uname -i`
machine_arch=`uname -m`


rm -f /etc/sysconfig/i18n || true
cat /etc/locale.conf
cat >/etc/locale.conf <<EOF
LANG=en_US.UTF-8
EOF
export LANG=en_US.UTF-8


rpm --quiet -q epel-release || yum install -y epel-release || rpm -U https://dl.fedoraproject.org/pub/epel/7/$architecture/e/epel-release-7-6.noarch.rpm
# RPMForge/RepoForge is a dead project. It is not maintained. DO NOT USE. https://github.com/repoforge/rpms/issues/375
rpm --quiet -q rpmforge-release && rpm --quiet -e rpmforge-release
# rpm --quiet -q rpmforge-release || rpm -U http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el7.rf.$machine_arch.rpm
# rpm --quiet -q rpmforge-release || rpm -U ftp://fr2.rpmfind.net/linux/dag/redhat/el7/en/$machine_arch/dag/RPMS/rpmforge-release-0.5.3-1.el7.rf.$machine_arch.rpm
rpm --quiet -q puppet6-release || rpm -U https://yum.puppet.com/puppet6-release-el-7.noarch.rpm

# yum remove -q -y sendmail
yum install -y ed wget rsync sudo cronie patch man lsof telnet ruby


# Enable Epel (enabled = 1)
echo '/name *= *.*Extra Packages.*basearch *$/
/^enabled *= *
s/=.*/=1/
wq
' |ed /etc/yum.repos.d/epel.repo

sed -i 's/exclude *= *tor//' /etc/yum.repos.d/epel.repo
echo '/\[epel\]/
a
exclude=tor
.
wq
' |ed /etc/yum.repos.d/epel.repo


if test -n "$etc_hosts"; then
    echo "$etc_hosts" |tr ':' ' ' |tr '+' ' ' |tr ',' "\n" >>/etc/hosts
fi
cat /etc/hosts


# Install Puppet 6 agent
puppet_version='6.6.0'; yum install -y puppet-agent-${puppet_version}


yum clean packages || true


exit $rval
